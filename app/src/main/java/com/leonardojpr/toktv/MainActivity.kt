package com.leonardojpr.toktv

import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.util.Log
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import tv.tok.TokTv
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        TokTv.onActivityCreate(this, savedInstanceState)
        TokTv.setButtonVisibility(true)
        TokTv.showSocialPanel()
        TokTv.setInjectedAuthHandler(MyTokTvInjectedAuthHandler())

        try {
            val info = packageManager.getPackageInfo(
                    "com.leonardojpr.toktv",
                    PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {
        } catch (e: NoSuchAlgorithmException) {
        }

    }

    override fun onResume() {
        super.onResume()
        TokTv.onActivityResume(this)
        // your code, if needed
    }

    override fun onPause() {
        super.onPause()
        TokTv.onActivityPause(this)
        // your code, if needed
    }

    override fun onDestroy() {
        super.onDestroy()
        TokTv.onActivityDestroy(this)
        // your code, if needed
    }

    override fun onBackPressed() {
        if (!TokTv.onActivityBackPressed(this)) {
            // your code, if needed
            super.onBackPressed()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (!TokTv.onActivityResult(this, requestCode, resultCode, data)) {
            // your code, if needed
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        TokTv.onActivityConfigurationChanged(this, newConfig)
    }
}
