package com.leonardojpr.toktv;

import tv.tok.TokTvInjectedAuthHandler;

public class MyTokTvInjectedAuthHandler implements TokTvInjectedAuthHandler {

    @Override
    public void onLoginActionRequested() {
        // Perform your login procedure, and then call one or more of the following:
        // TokTv.setUserIdentity(TokTv.IDENTITY_PROVIDER_FACEBOOK, fbAccessToken);
        // TokTv.setUserIdentity(TokTv.IDENTITY_PROVIDER_ACCESS_TOKEN, yourAccessToken);
    }

    @Override
    public void onLogoutActionRequested() {
        // Perform your own logout procedure, and then call one or more of the following:
        // TokTv.clearUserIdentity(TokTv.IDENTITY_PROVIDER_FACEBOOK);
        // TokTv.clearUserIdentity(TokTv.IDENTITY_PROVIDER_ACCESS_TOKEN);
    }

}