package com.leonardojpr.toktv

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import tv.tok.TokTv

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        FacebookSdk.sdkInitialize(this)
        AppEventsLogger.activateApp(this)
        TokTv.onApplicationCreate(this)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onTerminate() {
        super.onTerminate()
        TokTv.onApplicationTerminate(this)
    }
}